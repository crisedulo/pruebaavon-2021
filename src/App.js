import React, { Fragment } from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import Header from './components/Header'
import Title from './components/Title'
import Search from './components/Search'
import CustomTable from './components/CustomTable'

export default function App () {
  return (
    <Fragment>
      <Header />
      <CssBaseline />
      <Title text='INFORMACION DEL CLIMA' />
      <Search />
      <CustomTable />
    </Fragment>
  )
}
