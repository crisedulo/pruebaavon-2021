import React, { useContext } from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableFooter from '@material-ui/core/TableFooter'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import TablePaginationActions from './TablePaginationActions'
import { WeatherContext } from '../context/WeatherContext'

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: '#ef6c00',
    fontFamily: 'Arial',
    fontSize: '20px',
    color: theme.palette.common.white,
    fontStyle: 'normal',
    fontDisplay: 'swap',
    fontWeight: 'bold'
  },
  body: {
    fontSize: 14,
    fontFamily: 'Arial',
    fontWeight: 'bold'
  }
}))(TableCell)

const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(6)
  },
  table: {
    minWidth: 500
  }
}))

export default function CustomTable () {
  const classes = useStyles()
  const [page, setPage] = React.useState(0)
  const [rowsPerPage, setRowsPerPage] = React.useState(5)
  const { city } = useContext(WeatherContext)

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, city.length - page * rowsPerPage)

  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  return (
    <div className={classes.root}>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label='custom pagination table'>
          <TableHead>
            <TableRow>
              <StyledTableCell style={{ width: '50%' }} align='center'>
                DEPARTAMENTO
              </StyledTableCell>
              <StyledTableCell style={{ width: '50%' }} align='center'>
                CLIMA
              </StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {(rowsPerPage > 0
              ? city.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              : city
            ).map((row, index) => (
              <TableRow
                key={index}
                style={index % 2 === 0 ? { backgroundColor: '#fff3e0' } : null}
              >
                <TableCell
                  style={{
                    width: 100,
                    fontSize: '16px',
                    fontWeight: 'bold',
                    fontFamily: 'Arial',
                    border: 'solid',
                    borderWidth: 'thin',
                    borderColor: '#ef6c00'
                  }}
                  align='left'
                >
                  {row.city_name}
                </TableCell>
                <TableCell
                  style={{
                    width: 100,
                    fontSize: '16px',
                    fontWeight: 'bold',
                    fontFamily: 'Arial',
                    border: 'solid',
                    borderWidth: 'thin',
                    borderColor: '#ef6c00'
                  }}
                  align='left'
                >
                  {row.temp}
                </TableCell>
              </TableRow>
            ))}

            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                colSpan={3}
                count={city.length}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  inputProps: { 'aria-label': 'rows per page' },
                  native: true
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
                ActionsComponent={TablePaginationActions}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
    </div>
  )
}
