import React, { Fragment } from 'react'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  image: {
    width: '100%'
  }
}))

export default function Header () {
  const classes = useStyles()

  return (
    <Fragment>
      <img className={classes.image} src={'avon.png'} alt='Avon' />
    </Fragment>
  )
}
