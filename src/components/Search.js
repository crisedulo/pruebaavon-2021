import React, { useState, useContext } from 'react'
import { TextField, Grid, makeStyles, Button } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import useFetch from 'use-http'
import { WeatherContext } from '../context/WeatherContext'

const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(6)
  },
  button: {
    marginTop: '6px',
    backgroundColor: '#ef6c00',
    color: theme.palette.common.white,
    '&:hover': {
      backgroundColor: '#ffd180',
      color: theme.palette.common.white
    }
  }
}))

export default function Search () {
  const classes = useStyles()
  const { updateCity } = useContext(WeatherContext)
  const [valueDep, setValueDep] = useState(null)
  const { get } = useFetch(process.env.REACT_APP_API_WEATHER)

  const handleSearch = async () => {
    const weatherCity = await get(
      `/current?city=${valueDep.city}&country=GT&key=${process.env.REACT_APP_KEY}`
    )
    if (weatherCity.data) {
      const { data } = weatherCity
      //Utilizamos contexto para evitar pasar Props de componente a componente
      updateCity(...data)
    }
  }

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={12} sm={3}>
          <Autocomplete
            id='combo-box-demo'
            value={valueDep}
            onChange={(event, newValue) => {
              setValueDep(newValue)
            }}
            options={citysGuatemala}
            getOptionLabel={option => option.name}
            style={{ width: 279 }}
            renderInput={params => (
              <TextField {...params} label='Departamento' variant='outlined' />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={3}>
          <Button
            className={classes.button}
            onClick={handleSearch}
            variant='contained'
          >
            Buscar
          </Button>
        </Grid>
      </Grid>
    </div>
  )
}

// Cuidades de Guatemala Departamentos
const citysGuatemala = [
  { name: 'Alta Verapaz', city: 'Coban' },
  { name: 'Baja Verapaz', city: 'Salama' },
  { name: 'Chimaltenango', city: 'Chimaltenango' },
  { name: 'Chiquimula', city: 'Chiquimula' },
  { name: 'El Progreso', city: 'Guastatoya' },
  { name: 'Escuintla', city: 'Escuintla' },
  { name: 'Guatemala', city: 'Guatemala' },
  { name: 'Huehuetenango', city: 'Huehuetenango' },
  { name: 'Izabal', city: 'Izabal' },
  { name: 'Jalapa', city: 'Jalapa' },
  { name: 'Jutiapa', city: 'Jutiapa' },
  { name: 'Petén', city: 'Flores' },
  { name: 'Quetzaltenango', city: 'Quetzaltenango' },
  { name: 'Quiché', city: 'Quiché' },
  { name: 'Retalhuleu', city: 'Retalhuleu' },
  { name: 'Sacatepéquez', city: 'Sacatepéquez' },
  { name: 'San Marcos', city: 'San Marcos' },
  { name: 'Santa Rosa', city: 'Cuilapa' },
  { name: 'Sololá', city: 'Sololá' },
  { name: 'Suchitepéquez', city: 'Suchitepéquez' },
  { name: 'Totonicapán', city: 'Totonicapán' },
  { name: 'Zacapa', city: 'Zacapa' }
]
