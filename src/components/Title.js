import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  text: {
    fontSize: '20px',
    fontFamily: 'Rockwell',
    fontWeight: 'bold',
    color: 'red',
    textAlign: 'center',
    marginTop: theme.spacing(2)
  }
}))

export default function Title ({ text }) {
  const classes = useStyles()
  return <div className={classes.text}>{text}</div>
}
