import React, { createContext, useState } from 'react'

export const WeatherContext = createContext()

export const WeatherProvider = ({ children }) => {
  const [city, setCity] = useState([])

  const updateCity = element => {
    setCity([...city, element])
  }

  return (
    <WeatherContext.Provider
      value={{
        city,
        updateCity
      }}
    >
      {children}
    </WeatherContext.Provider>
  )
}
